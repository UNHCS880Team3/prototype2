import pandas as pd
import csv
import re
import datetime
import os

timeInterval = re.compile(r'\d\d\d\d[-]\d\d[-]\d\dT((\d\d)[:](\d\d)[:](\d\d))')
mmsInterval = re.compile(r'\d\d\d\d[-]\d\d[-]\d\d ((\d\d)[:](\d\d)[:](\d\d)[.](\d+))')


def create_timeFrame(stilF):
    timeFrame = pd.DataFrame()
    print("Reading stil file".format(stilF))
    mmS = int(0)
    with open(stilF, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        startT = []
        endT = []
        priority = []
        for line in csv_reader:
            HourS = int(timeInterval.search(line['Start']).group(2))
            MinS = int(timeInterval.search(line['Start']).group(3))
            SecS = int(timeInterval.search(line['Start']).group(4))

            HourE = int(timeInterval.search(line['End']).group(2))
            MinE = int(timeInterval.search(line['End']).group(3))
            SecE = int(timeInterval.search(line['End']).group(4))

            startObj = datetime.time(HourS, MinS, SecS, mmS)
            endObj = datetime.time(HourE, MinE, SecE, mmS)
            startT.append(startObj)
            endT.append(endObj)
            pri = float(line['Priority'])
            priority.append(pri)
        # startT.sort()
        # endT.sort()
    try:
        timeFrame = pd.DataFrame({'start_time': startT, 'end_time': endT, 'priority': priority},
                                 columns=['start_time', 'end_time', 'priority'])
    except Exception as e:
        print("Error in creating data frame")
    print("Returning frames")
    return timeFrame


def return_interval(stil, time):
    ret = 0
    for i in range(0, len(stil)):
        if time >= stil['start_time'][i] and time <= stil['end_time'][i]:
            ret = 1
            return ret, stil['priority'][i]
        else:
            ret = 0

    return ret, 0


def write_csv(fr, outFile):
    fr.to_csv(outFile, sep=',', index=False)


def read_data(mmsF, stilF, outFile):
    print("Creating time interval time...")
    frame = create_timeFrame(stilF)
    print(frame)
    print("Creating time interval completed...")

    target = list()
    prior =list()
    ob = 0
    p = 0
    print("Reading mms file {} ...".format(mmsF))
    with open(mmsF, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)

        for line in csv_reader:
            Hour = int(mmsInterval.search(line['Time']).group(2))
            Minute = int(mmsInterval.search(line['Time']).group(3))
            Sec = int(mmsInterval.search(line['Time']).group(4))
            mmSearch = mmsInterval.search(line['Time']).group(5)
            mmsFloating = "." + mmSearch
            mmS = int(float(mmsFloating) * 1000)  # time constructor accepts the time in Micro seconds.
            timeObj = datetime.time(Hour, Minute, Sec, mmS)

            observed, prio = return_interval(frame, timeObj)
            if prio is not 0:
                p+=1
            prior.append(prio)

            if observed == 1:
                ob += 1
            target.append(observed)

    target_attached = pd.read_csv(mmsF, index_col=False)
    column_values = pd.Series(target)
    priority_values= pd.Series(prior)
    target_attached['SITLTarget'] = column_values.values
    target_attached['Priority']=priority_values.values
    write_csv(target_attached, outFile)
    print("writing target into new output file {}".format(outFile))
    print("Number of observed targets in the interval -->{}".format(ob))
    print("percentage of observed targets in the interval -->{:.2f}".format((ob / len(target)) * 100) + "%")
    print("Total length of the data points -->{}".format(len(target)))
    print("Value of priority is {}".format(p))

def mergeFile(file1, file2):
    fileone = pd.read_csv(file1).drop(['Unnamed: 0'],axis=1)
    filetwo= pd.read_csv(file2).drop(['Unnamed: 0'],axis=1)
    #result= fileone.append(filetwo,ignore_index=True)
    #result.to_csv('mms_merged_priority_target.csv', sep=',', index=False)
    #print(fileone)
    #print(filetwo)
    filename1 = file1.find(".")
    filename1= file1[1:filename1]

    filename2 = file2.find(".")
    filename2 = file2[1:filename2]

    filename1=filename1+"_unindexed.csv"
    filename2=filename2+"_unindexed.csv"

    fileone.to_csv(filename1,sep=',',index=False)
    filetwo.to_csv(filename2,sep=',',index=False)

    df1= pd.read_csv(filename1)
    df2=pd.read_csv(filename2)
    result =df1.append(df2,ignore_index=True)
    result.to_csv('mms_merged_priority_target.csv', sep=',', index=True)
    os.remove(filename1)
    os.remove(filename2)