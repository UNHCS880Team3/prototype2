import argparse
import readData as r
import time

parser =argparse.ArgumentParser(description="Magnetospheric Multiscale Mission (MMS) Target builder")
parser.add_argument('-m','--mms',help='Pass the MMS file')
parser.add_argument('-s','--stil',help='Pass the STIL file')
parser.add_argument('-o','--output',help='out csv filename')
parser.add_argument('-d1','--data1',help='Datafile11')
parser.add_argument('-d2','--data2',help='Datafile2')
args= parser.parse_args()

mmsFile=''
stilFile=''
outFile=''

if args.data1:
    r.mergeFile(args.data1,args.data2)
else:

    if args.mms:
        mmsFile= args.mms
    else:
        mmsFile= 'mms_20151016.csv'

    if args.stil:
        stilFile= args.stil
    else:
        stilFile= 'sitl_20151016.csv'
    if args.output:
        outFile=args.output
    else:
        outFile='mms_updated_target.csv'
    t1= time.time()
    r.read_data(mmsFile,stilFile,outFile)
    t2= time.time()
    print("Time took:{:.2f}sec".format(t2-t1))


