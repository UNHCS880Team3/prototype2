# Including libraries
library(tree)
library(ISLR)

# Preparing the data
spaceData <- read.csv("mms_updated_target.csv",header=T,na.strings="?")
spaceData=na.omit(spaceData)
#fix(spaceData)
names(spaceData)
summary(spaceData)
dim(spaceData)
spaceData.numeric = data.matrix(spaceData[,1:17])
dim(spaceData.numeric)
cor(spaceData.numeric)
y = spaceData$SITLTarget
x = spaceData.numeric
sDataFrame=data.frame(x=x, y=as.factor(y))
train=sample(1:nrow(sDataFrame), nrow(sDataFrame)*3/4)
sDataFrame.test=sDataFrame[-train,]
y.test=y[-train]

# Fit a tree to the data
tree.spaceData=tree(y~.,sDataFrame)
summary(tree.spaceData)
plot(tree.spaceData)
tree.spaceData

tree.spaceData=tree(y~.,sDataFrame,subset=train)
tree.pred=predict(tree.spaceData,sDataFrame.test,type="class")
tree.table = table(predict=tree.pred, truth=y.test)
cat("\ntree test error is:", 1 - (tree.table[1,1] + tree.table[2,2])/length(tree.pred))

# Use cross-validation to the tree
cv.spaceData=cv.tree(tree.spaceData,FUN=prune.misclass)
cv.spaceData # dev attribute stores cv error
bestTerminalN = which(cv.spaceData$dev == min(cv.spaceData$dev))
cat("\nOptimum number of terminal points is: ", cv.spaceData$size[bestTerminalN])
prune.spaceData=prune.misclass(tree.spaceData,best=cv.spaceData$size[bestTerminalN])
plot(prune.spaceData)
tree.pred = predict(prune.spaceData,sDataFrame.test,type="class")
tree.table = table(predict=tree.pred, truth=y.test)
cat("\nThe prunned tree has a test error of:", 1 - (tree.table[1,1] + tree.table[2,2])/length(tree.pred))

# Bagging

library(randomForest)
#rf.spaceData = tree(y~.,sDataFrame,subset=train)
bag.spaceData = randomForest(y~.,data=sDataFrame,subset=train,mtry=(ncol(sDataFrame) - 2),importance=TRUE) # mtry=... indicates that all predictors should be considered for each split of the tree, and this means we are doing bagging
bag.spaceData
bag.pred = predict(bag.spaceData,newdata=sDataFrame[-train,])
mean((bag.pred-(y.test))^2)
bagging.table = table(predict=bag.pred, truth=y.test)
cat("\nBagging test error is:", 1 - (bagging.table[1,1] + bagging.table[2,2])/length(bag.pred))
bag.spaceData=randomForest(y~.,data=sDataFrame,subset=train,mtry=(ncol(sDataFrame) - 2),ntree=25) # ntree changes the number of trees grown by randomForest
bag.pred = predict(bag.spaceData,newdata=sDataFrame[-train,])
mean((yhat.bag-y.test)^2)
bagging.table = table(predict=bag.pred, truth=y.test)
cat("\nBagging test with mtree=25 error is:", 1 - (bagging.table[1,1] + bagging.table[2,2])/length(bag.pred))

# Random Forests

rf.spaceData=randomForest(y~.,data=sDataFrame,subset=train,mtry=sqrt((ncol(sDataFrame)-2)),importance=TRUE)
rf.pred = predict(bag.spaceData,newdata=sDataFrame[-train,])
mean((rf.pred-y.test)^2)
importance(rf.spaceData)
varImpPlot(rf.spaceData)
tf.table = table(predict=rf.pred, truth=y.test)
cat("\nRandom Forests test error is:", 1 - (tf.table[1,1] + tf.table[2,2])/length(rf.pred))

# Boosting

library(gbm)
boost.spaceData=gbm(y~.,data=sDataFrame[train,],distribution="bernoulli",n.trees=5000, interaction.depth=4)
summary(boost.spaceData)
boost.pred=predict(boost.spaceData,newdata=sDataFrame[-train,],n.trees=5000)
mean((boost.pred-y.test)^2)
boost.spaceData=gbm(y~.,data=sDataFrame[train,],distribution="gaussian",n.trees=5000,interaction.depth=4,shrinkage=0.2,verbose=F)
boost.pred=predict(boost.spaceData,newdata=sDataFrame[-train,],n.trees=5000)
mean((boost.pred-y.test)^2)
boost.table = table(predict=boost.pred, truth=y.test)
cat("\nRandom Forests test error is:", 1 - (boost.table[1,1] + boost.table[2,2])/length(boost.pred))


# Support Vector Classifier

library(e1071)
svm.spaceData=svm(y~., data=sDataFrame[train,], kernel="radial", gamma = 1.9, cost=10,scale=FALSE)
svm.pred = predict(bag.spaceData,newdata=sDataFrame[-train,])
svm.table = table(predict=svm.pred, truth=y.test)
cat("\nSVM test error is:", 1 - (svm.table[1,1] + svm.table[2,2])/length(svm.pred))

svm.spaceData$index
summary(svm.spaceData)
tune.out=tune(svm,y~.,data=sDataFrame[train,],kernel="radial",ranges=list(cost=c(0.001, 0.01, 0.1, 1,5,10,100)))
svm.pred = predict(bag.spaceData,newdata=sDataFrame[-train,])
summary(tune.out)
bestmod=tune.out$best.model
summary(bestmod)
svm.table = table(predict=svm.pred, truth=y.test)
cat("\nRandom Forests test error is:", 1 - (svm.table[1,1] + svm.table[2,2])/length(svm.pred))