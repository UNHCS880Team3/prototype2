import argparse
import matplotlib.pyplot as plt
import pandas as pd
parser =argparse.ArgumentParser(description="Magnetospheric Multiscale Mission graph builder")
parser.add_argument('-m','--mms',help='Updated Target file')
parser.add_argument('-x','--xaxis',help='x axis')
parser.add_argument('-y','--yaxis',help='y axis')
args= parser.parse_args()


def setupGraph(frame,xCol,yCol):
    x=frame[xCol].tolist()
    y=frame[yCol].tolist()
    plt.scatter(x,y)
    plt.xlabel(xCol)
    plt.ylabel(yCol)
    plt.title(xCol+" Vs "+yCol)
    plt.show()


frame= pd.DataFrame()
x=''
y=''
if args.mms:
    frame = pd.read_csv(args.mms,sep=',')

if args.xaxis:
    x= args.xaxis

if args.yaxis:
    y = args.yaxis

setupGraph(frame,x,y)






